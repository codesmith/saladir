/*
 * Automatically generated version header
 * for the legend of saladir
 * (C) 1997/1998 by Erno Tuomainen
 *
 */

#define PROC_VERSION "V0.34.32"
#define PROC_DATE "(Sun Apr 25 18:25:14 1999)"
#ifdef linux
#define PROC_PLATFORM "-Linux"
#else
#define PROC_PLATFORM "-DOS"
#endif
